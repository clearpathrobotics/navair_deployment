#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <hector_uav_msgs/PoseAction.h>
#include <hector_uav_msgs/LandingAction.h>
#include <hector_quadrotor_controller/helpers.h>
#include <navair_actions/base_action.h>

namespace navair_actions
{

  class LandingActionServer{

  public:

    LandingActionServer(ros::NodeHandle nh)
        : landing_server_(nh, "action/landing", boost::bind(&LandingActionServer::landingActionCb, this, _1)),
          pose_client_(nh, "action/pose")
    {
      nh.param<double>("action_frequency", frequency_, 10.0);
      nh.param<double>("landing_height", landing_height_, 0.3);
      nh.param<double>("connection_timeout", connection_timeout_, 10.0);
      nh.param<double>("action_timout", action_timeout_, 30.0);

      if(!pose_client_.waitForServer(ros::Duration(connection_timeout_))){
        ROS_ERROR_STREAM("Could not connect to " << nh.resolveName("action/pose"));
      }
    }

    void landingActionCb(const hector_uav_msgs::LandingGoalConstPtr &goal)
    {

      hector_uav_msgs::PoseGoal pose_goal;
      if(!goal->landing_zone.header.frame_id.empty()){
        pose_goal.target_pose = goal->landing_zone;
      }else{
        pose_goal.target_pose = *landing_server_.getPose();
      }
      pose_goal.target_pose.pose.position.z = std::min(landing_height_, pose_goal.target_pose.pose.position.z);
      pose_client_.sendGoal(pose_goal);
      pose_client_.waitForResult(ros::Duration(action_timeout_));

      if(pose_client_.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
        if(landing_server_.enableMotors(false))
        {
          landing_server_.get()->setSucceeded();
          ROS_WARN("Landing succeeded");
          return;
        }
      }
      ROS_WARN("Landing failed");
      landing_server_.get()->setAborted();

    }

  private:

    actionlib::SimpleActionClient<hector_uav_msgs::PoseAction> pose_client_;
    navair_actions::BaseActionServer<hector_uav_msgs::LandingAction> landing_server_;
    ros::Publisher pose_pub_;

    double frequency_, landing_height_, connection_timeout_, action_timeout_;

  };

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "takeoff_action");

  ros::NodeHandle nh;
  navair_actions::LandingActionServer server(nh);

  ros::spin();

  return 0;
}
