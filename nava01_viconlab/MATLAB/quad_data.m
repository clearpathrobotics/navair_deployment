function quad_data

    % Setup
    setenv('ROS_IP','192.168.127.1')
    rosinit('ib-T440p')

    %Subscribe to each of the quadrotor position topics
    uav1  = rossubscriber('/uav1/state_to_tf/pose');
    uav6  = rossubscriber('/uav6/state_to_tf/pose');
    uav8  = rossubscriber('/uav8/state_to_tf/pose');
    uav10 = rossubscriber('/uav10/state_to_tf/pose');

    % Create a wind publisher to simulate disturbance
    wind_pub = rospublisher('/wind');
    wind_msg = rosmessage(wind_pub);

    % MATLAB UI control definitions for wind
    sld = uicontrol('Style', 'slider',...
            'Min',-4,'Max',4,'Value',0,...
            'Position', [400 10 120 20],...
            'Callback', @wind);

    % MATLAB UI control definitions for wind
    txt = uicontrol('Style','text',...
            'Position',[400 35 120 20],...
            'String','X Wind speed in m/s');
    tic;
    % Start timer, and execute script for 60 seconds
    while toc < 60
        % Collect data from ROS
        uav1_pose = receive(uav1);
        uav6_pose = receive(uav6);
        uav8_pose = receive(uav8);
        uav10_pose = receive(uav10);

        %Plot data on graph, keeping old data and axis
        hold on;
        grid on;
        axis([-1.5 1.5 -1.5 1.5 0 1.5])
        scatter3(uav1_pose.Pose.Position.X,uav1_pose.Pose.Position.Y,uav1_pose.Pose.Position.Z,1,'r')
        scatter3(uav6_pose.Pose.Position.X,uav6_pose.Pose.Position.Y,uav6_pose.Pose.Position.Z,1,'g')
        scatter3(uav8_pose.Pose.Position.X,uav8_pose.Pose.Position.Y,uav8_pose.Pose.Position.Z,1,'blue')
        scatter3(uav10_pose.Pose.Position.X,uav10_pose.Pose.Position.Y,uav10_pose.Pose.Position.Z,1,'black')
    end

    % Function to take in MATLAB UI data, and send to ROS
    function wind(source,callbackdata)
        wind_msg.X = source.Value;
        send(wind_pub,wind_msg);
    end

    rosshutdown
end
