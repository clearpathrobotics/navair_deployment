#include <controller_interface/controller.h>
#include <geometry_msgs/TwistStamped.h>
#include <hector_quadrotor_controller/quadrotor_interface.h>
#include <ros/subscriber.h>
#include <hector_quadrotor_controller/limiters.h>
#include <boost/thread/mutex.hpp>
#include <std_msgs/Float64.h>
#include <pluginlib/class_list_macros.h>

namespace navair_controllers
{
  using namespace hector_quadrotor_controller;
  class PassThroughController : public controller_interface::Controller<hector_quadrotor_controller::QuadrotorInterface>
  {
  public:

    PassThroughController() { }
    virtual ~PassThroughController()  { }

    virtual bool init(hector_quadrotor_controller::QuadrotorInterface *interface,
              ros::NodeHandle &root_nh,
              ros::NodeHandle &controller_nh)
    {
      pose_ = interface->getPose();
      twist_ = interface->getTwist();

      twist_input_ = interface->addInput<TwistCommandHandle>("twist");
      twist_output_ = interface->addOutput<TwistCommandHandle>("twist_scaled");

      controller_nh.subscribe<std_msgs::Float64>("scale", 1, boost::bind(
          &PassThroughController::scaleCb, this, _1));

      return true;
    }

    virtual void starting(const ros::Time &time)
    {
      twist_output_->start();
      scale_ = 1.0;
    }

    virtual void stopping(const ros::Time &time)
    {
      twist_output_->stop();
    }

    virtual void update(const ros::Time &time, const ros::Duration &period)
    {

      if (twist_input_->connected() && twist_input_->enabled())
      {
        geometry_msgs::Twist twist_command = twist_input_->getCommand();

        boost::mutex::scoped_lock lock(scale_mutex_);
        twist_command.linear.x *= scale_;
        twist_command.linear.y *= scale_;

        twist_output_->setCommand(twist_command);
      }
    }

  private:

    void scaleCb(const std_msgs::Float64ConstPtr &msg)
    {
      boost::mutex::scoped_lock lock(scale_mutex_);
      scale_ = msg->data;
    }

    PoseHandlePtr pose_;
    TwistHandlePtr twist_;
    TwistCommandHandlePtr twist_input_, twist_output_;
    ros::Subscriber scale_subscriber_;

    boost::mutex scale_mutex_;
    double scale_;
 };
} // namespace navair_controllers
PLUGINLIB_EXPORT_CLASS(navair_controllers::PassThroughController, controller_interface::ControllerBase)
