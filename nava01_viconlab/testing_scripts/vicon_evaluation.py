#!/usr/bin/python
from tf.transformations import euler_from_quaternion
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src")
from bag_helpers import *
from graphing_helpers import *
import rosbag
from pandas import DataFrame
from numpy import mean

def parse_bag(filename, timelimit = 1e9):
    bag = rosbag.Bag(filename)
    #get the position info
    pose_dataframe = []
    distance_error = []
    first_time = -1
    seconds = 0
    for topic,message,time in bag.read_messages():
        if first_time <= 0:
            first_time = time.to_sec()
        if topic == "/vicon/wand/wand":
            trans = message.transform.translation
            rot = message.transform.rotation
            seconds = time.to_sec()-first_time
            angles = euler_from_quaternion([rot.x,rot.y,rot.z,rot.w])
            pose = {'clock':seconds,'x':trans.x, 'y':trans.y, 'z':trans.z,'pitch':angles[0],'roll':angles[1],'yaw':angles[2]}
            pose_dataframe.append(pose)
        if seconds>timelimit:
            print str(seconds)+" over limit"
            break
    pose_dataframe = DataFrame(pose_dataframe)
    return pose_dataframe

def distance(dataframe):
    mean_x = mean(dataframe['x'])
    mean_y = mean(dataframe['y'])
    mean_z = mean(dataframe['z'])
    distances = []
    for i in range(0,len(dataframe)):
        distance = ((dataframe.loc[i,'x']-mean_x)**2 + (dataframe.loc[i,'y']-mean_y)**2.0 \
        + (dataframe.loc[i,'z']-mean_z)**2)**0.5
        distances.append(distance*1000.0)
    return distances

if __name__ == "__main__":
    vicon_filename = sys.argv[1]
    #check the bag file
    _required_topics = ["/vicon/wand/wand"]
    if not required_topics(vicon_filename, _required_topics):
        sys.exit()
    wand_pose_dataframe = parse_bag(vicon_filename)
    wand_distance = distance(wand_pose_dataframe)
    histogram_chi2(wand_distance, "vicon_positional_error.png", "Vicon Positional Error", "mm")
    wand_yaw_error = (wand_pose_dataframe['yaw']-mean(wand_pose_dataframe['yaw']))*1000.0
    histogram_norm(wand_yaw_error, "vicon_yaw_error.png", "Vicon Orientation Error", "mrad")
