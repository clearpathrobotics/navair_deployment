### Set up ###

Clone to your computer, then run:
```
pip install requirements.txt
```
For the ipython notebooks, either run ipython notebook from the project directory, or create a symbolic link to the directory from your existing ipython notebook directory.

###
