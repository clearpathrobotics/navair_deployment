#!/usr/bin/python

from tf.transformations import euler_from_quaternion
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src")
from bag_helpers import *
from graphing_helpers import *
import rosbag
import rospy
from pandas import DataFrame
from numpy import mean, amax, sqrt, asarray, sin, cos
from copy import deepcopy

from math import atan, tan

import argparse

def parse_bag(filename, timelimit=1e9, uav_number=6, source_velocity = True):
    bag = rosbag.Bag(filename)
    first_time = 0
    seconds = 0
    #ground_truth_topic = "/uav"+str(uav_number)+"/ground_truth_to_tf/pose"
    ground_truth_topic = "/uav"+str(uav_number)+"/state_to_tf/pose"
    controller_topic = "/uav"+str(uav_number)+"/controller/position/$$/state"
    vel_controller_topic = "/uav"+str(uav_number)+"/controller/velocity/$$/state"
    #since there is no velocity controller, yaw will be a little different
    parts = ['x','y','z','yaw']
    controller_topics = []
    vel_controller_topics = []
    for part in parts:
        controller_topics.append(controller_topic.replace("$$",part))
        if not part == 'yaw':
            vel_controller_topics.append(vel_controller_topic.replace("$$",part))

    if not required_topics(filename, controller_topics+vel_controller_topics):
        return
    df_command = {}
    #stick the ground truth in the pose_dataframe, stick the velocity commands in velocity_dataframe
    pose_dataframe = []
    velocity_dataframe = []
    first_time = -1
    error = {}
    prev_yaw = 0
    prev_time = 0
    prev_yaw_vel = 0
    new_yaw_vel = 0

    for topic,message,time in bag.read_messages():
        if first_time <= 0:
            first_time = time.to_sec()
        if topic == ground_truth_topic:
            seconds = time.to_sec()-first_time
            ori = message.pose.orientation
            yaw = euler_from_quaternion([ori.x,ori.y,ori.z,ori.w])[2]
            px = message.pose.position.x
            py = message.pose.position.y
            pz = message.pose.position.z
            #calculate the ground truth velocity from the previous position
            if len(pose_dataframe)>0:
                df_prev = pose_dataframe[-1]
                vx = (px-df_prev['px'])/(seconds-df_prev['clock'])
                vy = (py-df_prev['py'])/(seconds-df_prev['clock'])
                vz = (pz-df_prev['pz'])/(seconds-df_prev['clock'])
                #yaw difference needs to be wrapped to an angle close to 0
                dyaw = atan(tan(yaw-df_prev['yaw']))
                vyaw = dyaw/(seconds-df_prev['clock'])
            else:
                vx = 0
                vy = 0
                vz = 0
                vyaw = 0
            df = {'clock': seconds,'px':px,'py':py,'pz':pz,'yaw':yaw, 'vx':vx, 'vy':vy, 'vz':vz, 'vyaw': vyaw}
            prev_yaw_vel = (yaw-prev_yaw)/(seconds-prev_time)
            prev_time = seconds
            prev_yaw = yaw
            pose_dataframe.append(df)
        if topic in controller_topics:
            seconds = time.to_sec()-first_time
            component = topic.split("/")[4]
            if not source_velocity:
                df_command['clock'] = seconds
                component = topic.split("/")[4]
                df_command[component] = message.command.real
                #only add to dataframe if there is an error
                if component in error.keys():
                    df_command[component+"_err"] = deepcopy(error[component])
                #yaw is different because of the controller
                if component == 'yaw':
                    dyaw = atan(tan(prev_yaw_vel-df_command[component]))
                    df_command['yaw_err'] = deepcopy(dyaw)
                velocity_dataframe.append(deepcopy(df_command))
        if topic in vel_controller_topics:
            seconds = time.to_sec() - first_time
            df_command[component+"_err"] = message.error.real
            component = topic.split("/")[4]
            if source_velocity:
                df_command['clock'] = seconds
                df_command[component] = message.command.real
                if component == 'yaw':
                    df_command[new_yaw_vel]
                    df_command['yaw_err'] = prev_yaw_vel - new_yaw_vel
                velocity_dataframe.append(deepcopy(df_command))
        if seconds>timelimit:
            print str(seconds)+" over limit"
            break
    pose_dataframe = DataFrame(pose_dataframe)
    velocity_dataframe = DataFrame(velocity_dataframe)
    return [pose_dataframe, velocity_dataframe]

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process the commanded and acheived velocities in a bag file.')
    parser.add_argument('--angular', nargs='+', help='The angular limit')
    parser.add_argument('--linear', nargs='+', help='The linear limit')
    parser.add_argument('--foldername', nargs='?', help='The foldername containing the bags')
    parser.add_argument('--filename', nargs='?', help='The filename for the bag')
    args = parser.parse_args()

    if not args.foldername is None:
        filenames = get_bags(args.foldername)
        filenames.sort()
        #check to make sure there are linear and angular limits for each file
        if len(args.linear)<len(filenames):
            raise ValueError("Insufficient linear limits declared!")
        if len(args.angular)<len(filenames):
            raise ValueError("Insufficient angular limits declared!")
    elif not args.filename is None:
        filenames = [args.filename]
    else:
        print "please specifiy a file"
        sys.exit()
    for i in range(0, len(filenames)):
        velocity_filename = filenames[i]
        _uav_number = guess_name(velocity_filename)

        print "found uav #"+str(_uav_number)
        linear_limit = None
        angular_limit = None
        #if the velocities are undefined, try to grab them off of an available rosmaster
        #assumes that x, y, and z are the same.
        if not args.angular[i] is None and not args.linear[i] is None:
            linear_limit = args.linear
            angular_limit = args.angular
        else:
            try:
                linear_limit = rospy.get_param("/uav"+_uav_number+"/limits/twist/linear/z/max")
                angular_limit = rospy.get_param("/uav"+_uav_number+"/limits/twist/angular/z/max")
            except KeyError:
                print "could not figure out velocity limits. fetching bag."
                [pose_dataframe, velocity_dataframe] = parse_bag(velocity_filename, uav_number = _uav_number)
                '''
                out_csv = velocity_filename.split("/")[-1]
                out_csv = out_csv.replace(".bag",".csv")
                print out_csv
                fp = open("pose_"+out_csv,"w+")
                fv = open("vel_"+out_csv,"w+")
                pose_dataframe.to_csv(fp)
                velocity_dataframe.to_csv(fv)
                fp.close()
                fv.close()
                '''
                linear_limit = amax(pose_dataframe['vz'])
                #print velocity_dataframe['z']
                angular_limit = amax(pose_dataframe['vyaw'])
                print "guessing linear limit: "+str(linear_limit)+" angular limit: "+str(angular_limit)
                #print velocity_dataframe
                components = ['x','y','z','yaw']
                for component in components:
                    out_filename = velocity_filename
                    out_filename = out_filename.replace(".bag",component+".png")
                    out_filename_time = out_filename.replace(".png", "_time.png")
                    paths = [[pose_dataframe['clock'],pose_dataframe['v'+component]],[velocity_dataframe['clock'],velocity_dataframe[component]]]
                    labels = ["Ground truth Velocity", "commanded velocity"]
                    time_trajectory(paths, labels, out_filename_time)
                    threshold = linear_limit
                    tmp_data = velocity_dataframe[component+"_err"][abs(velocity_dataframe[component])>0.9*threshold]
                    print "component: "+str(component)+"filtered: "+str(len(tmp_data))+" out of: "+str(len(velocity_dataframe[component+"_err"]))
                    #tmp_data = tmp_data[abs(tmp_data)<0.9*threshold]
                    histogram_norm(tmp_data, out_filename, x_title = component+" error", units = 'm', title = velocity_filename+" "+component)
        if not linear_limit is None and not angular_limit is None:

            [pose_dataframe, velocity_dataframe] = parse_bag(velocity_filename, uav_number = _uav_number, source_velocity = False)

            #combine x and y
            threshold = float(linear_limit[i])
            out_filename = velocity_filename
            out_filename = out_filename.replace(".bag","_XY.png")
            tmp_data_x = velocity_dataframe["x_err"][velocity_dataframe['x']*cos(pose_dataframe['yaw'])+velocity_dataframe['y']*sin(pose_dataframe['yaw'])>0.95*threshold]
            tmp_data_y = velocity_dataframe["y_err"][velocity_dataframe['x']*sin(pose_dataframe['yaw'])+velocity_dataframe['y']*cos(pose_dataframe['yaw'])>0.95*threshold]
            #for i in range(0, len(tmp_dat_))
            tmp_data = list(tmp_data_y)+list(tmp_data_x)
            tmp_data = [value for value in tmp_data if abs(value)<0.9*threshold]
            histogram_norm(tmp_data, out_filename, x_title = "Velocity error", units = 'm/s', title = "X/Y component, Speed limit: "+str(linear_limit[i])+" m/s")
            out_filename = velocity_filename
            out_filename = out_filename.replace(".bag","_Z.png")
            tmp_data = velocity_dataframe["z_err"][abs(velocity_dataframe['z'])>0.95*threshold]
            tmp_data = tmp_data[abs(tmp_data)<0.9*threshold]
            histogram_norm(tmp_data, out_filename, x_title = "Velocity error", units = 'm/s', title = "Z component, Speed limit: "+str(linear_limit[i])+" m/s")
            out_filename = velocity_filename
            out_filename = out_filename.replace(".bag","_yaw.png")
            threshold = float(angular_limit[i])
            tmp_data = velocity_dataframe["yaw_err"][abs(velocity_dataframe['yaw'])>0.95*threshold]
            tmp_data = tmp_data[abs(tmp_data)<0.9*threshold]
            histogram_norm(tmp_data, out_filename, x_title = "Velocity error", units = 'rad/s', title = "Yaw component, Speed limit: "+str(angular_limit[i])+" rad/s")
