import rospy
import rostopic
import actionlib
import sys
import numpy
import time

sys.path.append("/opt/ros/indigo/lib/actionlib/")
from dynamic_action import DynamicAction

from rosgraph_msgs.msg import Clock
from geometry_msgs.msg import TransformStamped, Twist, TwistStamped, Quaternion, Pose, PoseStamped
from nav_msgs.msg import Odometry
#remember to source your devel or this will not work! (it will pull in hector_uav_msgs from /ros)
from hector_uav_msgs.msg import PoseGoal, PoseAction, LandingGoal, LandingAction, TakeoffGoal, TakeoffAction

from hector_uav_msgs.srv import EnableMotors

from tf.transformations import euler_from_quaternion, quaternion_from_euler

from ros_helpers import *

from math import pi, asin, sin
from numpy import mean

class test_data:
    #Tests with multiple drones do not currently work.
    def __init__(self, verbose=False, uav_number=1, position_mode=True, vicon_topic = "/uav$$/state_to_tf/pose", action_topic = "/uav$$/action"):
        rospy.init_node('axclient', anonymous=True)
        #initialize the odometries
        self.groundtruths = [0,0,0]
        self.enableservices = 0
        self.verbose = verbose
        self.goal_state = False
        self.position_mode = position_mode
        self.uav_number = uav_number
        #measure the time
        self.clock = -1
        goal_topic = action_topic + "/pose/goal"
        landing_topic = action_topic + "/landing"
        takeoff_topic = action_topic + "/takeoff"

        action_topic = action_topic + "/pose"
        twist_topic = "/uav$$/command/twist"
        #first, make sure that all topics are available:
        topics = ["/clock"]
        topics.append(vicon_topic.replace("$$", str(uav_number)))
        topics.append(goal_topic.replace("$$", str(uav_number)))
        topics.append(action_topic.replace("$$", str(uav_number)))
        topics.append(landing_topic.replace("$$", str(uav_number)))
        topics.append(takeoff_topic.replace("$$", str(uav_number)))
        if not self.position_mode:
            topics.append(twist_topic.replace("$$", str(uav_number)))
        if check_topic_available(topics):
            print "All topics found"
        #if in position mode, use the action client for goals, if in velocity mode, send twist commands
        #to the twist topic
        if position_mode:
            self.goals = PoseGoal()
            self.actionclients = actionlib.SimpleActionClient(action_topic.replace("$$", str(self.uav_number)), PoseAction)
            self.landingclients = actionlib.SimpleActionClient(landing_topic.replace("$$", str(self.uav_number)), LandingAction)
            self.takeoffclients = actionlib.SimpleActionClient(takeoff_topic.replace("$$", str(self.uav_number)), TakeoffAction)
            self.landing_goals = LandingGoal()
            self.takeoff_goals = TakeoffGoal()
        else:
            self.goals = TwistStamped()
            self.velpublishers = rospy.Publisher(twist_topic.replace("$$",uav_number), TwistStamped, queue_size=10)
            self.enableservices = rospy.ServiceProxy('/uav'+uav_number+'/enable_motors', EnableMotors)
        #connect the ground truth topics to the subscriber
        self.listener(vicon_topic)
    def serverCheck(self):
        if self.position_mode:
            TIMEOUT = 0.01
            if self.actionclients.wait_for_server(rospy.Duration.from_sec(TIMEOUT)):
                print "Connected to pose server"
            else:
                print "Disconnected from pose server"
            if self.landingclients.wait_for_server(rospy.Duration.from_sec(TIMEOUT)):
                print "Connected to landing server"
            else:
                print "Disconnected from landing server"
            if self.takeoffclients.wait_for_server(rospy.Duration.from_sec(TIMEOUT)):
                print "Connected to takeoff server"
            else:
                print "Disconnected from takeoff server"
        else:
            TIMEOUT = 0.01
            if self.landingclients.wait_for_server(rospy.Duration.from_sec(TIMEOUT)):
                print "Connected to server"
            else:
                print "Disconnected from server"
    def takeoffGoal(self):
        self.takeoffclients.send_goal(self.takeoff_goals)
        self.takeoffclients.wait_for_result()
    def landGoal(self):
        self.landingclients.send_goal(self.landing_goals)
        self.landingclients.wait_for_result()
    def sendGoal(self, target):
        if self.verbose:
            print "Sending Goal"
        if self.position_mode:
            quaternion = quaternion_from_euler(target[3],target[4],target[5])
            self.goals.target_pose.pose.position.x = target[0]
            self.goals.target_pose.pose.position.y = target[1]
            self.goals.target_pose.pose.position.z = target[2]
            self.goals.target_pose.pose.orientation.x = quaternion[0]
            self.goals.target_pose.pose.orientation.y = quaternion[1]
            self.goals.target_pose.pose.orientation.z = quaternion[2]
            self.goals.target_pose.pose.orientation.w = quaternion[3]
            self.goals.target_pose.header.frame_id = "world"
            #we need to also get the attitude etc working
            #set the goal_status to true to indicate that a goal has been set
            #self.goal_state = True
            #self.actionclients.send_goal(self.goals,self.done_cb, self.active_cb,self.feedback_cb)
            self.actionclients.send_goal(self.goals)
            if self.verbose:
                print "Waiting for result"
            self.actionclients.wait_for_result()
            result = self.actionclients.get_result()
            if self.verbose:
                print "Result: "+str(result)
        else:
            self.goals.twist.linear.x = target[0]
            self.goals.twist.linear.y = target[1]
            self.goals.twist.linear.z = target[2]
            self.goals.twist.angular.z = target[3]
            self.velpublishers.publish(self.goals)
        #self.goal_state = False
    def done_cb(self, state, result):
        #this needs to change so that only certain actionclients are set to done
        if state==3:
            self.goal_state = False
        if self.verbose:
            print "state: "+str(state)+" result: "+str(result)
    def active_cb(self):
        if self.verbose:
            print "Goal is active"
    def feedback_cb(self, feedback):
        if self.verbose:
            print "feedback: "+str(feedback)
    def listener(self,topic):
        rospy.Subscriber("/clock",Clock, callback = self.callback_clock)
        #rospy.Subscriber("/uav"+str(i)+"/ground_truth/state",Odometry,lambda c: self.callback_odom(c, i))
        tmp_topic = topic.replace("$$",str(self.uav_number))
        rospy.Subscriber(tmp_topic,PoseStamped,callback = self.callback_odom)
    def callback_clock(self,data):
        self.clock = data.clock.secs+data.clock.nsecs*1e-9
        #print self.clock
    def callback_odom(self,data):
        #need to do some extra work to get quaternion out
        ori = data.pose.orientation
        angles = euler_from_quaternion([ori.x,ori.y,ori.z,ori.w])
        #there is no twist in real world
        self.groundtruths = [data.pose.position.x, data.pose.position.y, data.pose.position.z,\
         angles[0], angles[1], angles[2], \
         0, 0, 0, 0]
    #calculate the absolute distance between the current quadrotor location and the goal location
    def distance_measure(self,location):
        distance = ((location[0] - self.groundtruths[0])**2 + (location[1] - \
                    self.groundtruths[1])**2 + (location[2] - self.groundtruths[2])**2)**0.5
        return distance
    #test that the quadrotor can cover the test volume. Volume is a 3x2 list, where the first index is the dimension,
    #and the second dimension are the stop and end points
    def test_volume(self, volume):
        for i in range(0,2):
            for j in range(0,2):
                for k in range(0,2):
                    self.sendGoal([volume[0], volume[1][j], volume[2][k], 0, 0, 0])
        print "Volume Test Succeeded"
    def position_controller_test(self, component, start_point, end_point, num_samples):
        height = 0.5
        self.sendGoal([0,0,height,0,0,0])
        print "starting position controller test on drone "+str(num_samples)+" samples"
        distances = []
        #make a tmp goal with 0's for position, except at a height of 3 and angle
        tmp_goal = [0,0,height,0,0,0]
        #this needs to handle that the quadrotor is at 10 m up...
        initial_distance = self.groundtruths[component]
        for i in range(0,num_samples):
            tmp_goal[component]=start_point
            if i % 10==0:
                print "sample "+str(i)
            #wait until the drone is stopped
            while self.goal_state:
                time.sleep(0.01)
            #move the drone one path_size to the left in the axis
            self.sendGoal(tmp_goal)
            #wait until the drone is stopped
            while self.goal_state:
                time.sleep(0.01)
            #where did it actually get?
            distances.append(self.distance_measure(tmp_goal))
            tmp_goal[component]=end_point
            #move the drone one path_size to the right in the axis
            self.sendGoal(tmp_goal)
            #wait until the drone is stopped
            while  self.goal_state:
                time.sleep(0.01)
            #where did it actually get?
            distances.append(self.distance_measure(tmp_goal))
        return distances
    #test the position hold at multiple locations in a 3-D grid
    #volume is a 3x2 list describing the limits of the volume in 3 dimensions
    #num_samples are the number of samples along each axis.
    def position_grid_test(self,  volume, num_samples):
        distances = []
        height = 0.50
        num_time_samples = 10.0
        time_spacing = 0.1
        #generate the ranges
        for i in range(0,3):
            step = (volume[i][1]-volume[i][0])/num_samples
            distances.append(numpy.arange(volume[i][0], volume[i][1], step))
        errors = []
        for i in range(0, int(num_samples)):
            y_errors = []
            for j in range(0, int(num_samples)):
                z_errors = []
                for k in range(0, int(num_samples)):
                    goal = [distances[0][i], distances[1][j], distances[2][k]+height, 0, 0, 0]
                    if self.verbose:
                        print goal
                    self.sendGoal( goal)
                    dist_errors = []
                    #wait for a bit before starting the timing measurements
                    #time.sleep(1.0)
                    for m in range(0,int(num_time_samples)):
                        dist_errors.append(self.distance_measure( goal))
                        time.sleep(time_spacing)
                    z_errors.append(numpy.mean(dist_errors))
                y_errors.append(z_errors)
            errors.append(y_errors)
        return errors
    #test the angle hold at multiple locations in a circle
    #num_samples are the number of samples along the permiter of the circle.
    def angle_grid_test(self, num_samples, height):
        distances = []
        num_time_samples = 10.0
        time_spacing = 0.1
        upper_angle = 2.0*pi
        #generate the ranges
        step = (upper_angle-0.0)/num_samples
        angles = numpy.arange(0.0, upper_angle, step)
        errors = []
        for i in range(0, int(num_samples)):
            goal = [0, 0, height, 0, 0, angles[i]]
            if self.verbose:
                print goal
            self.sendGoal( goal)
            dist_errors = []
            #wait for a bit before starting the timing measurements
            #time.sleep(1.0)
            for m in range(0,int(num_time_samples)):
                #make sure the angle wraps
                angle_error = asin(sin(self.groundtruths[5]-angles[i]))
                dist_errors.append(angle_error)
                time.sleep(time_spacing)
            errors.append(mean(dist_errors))
        return errors
    #spin continuously
    def angle_spin_test(self):
        num_turns = 10
        for i in range(0,num_turns):
            self.sendGoal([0, 0, 0.5, 0, 0, 5*pi])
            self.sendGoal([0, 0, 0.5, 0, 0, 0])

    #need to ammend this to automatically stop once the quadrotor gets outside of a certain range
    def velocity_controller_test(self,component,target_speed,num_samples):
        print "starting velocity controller test on drone component: "+str(component)\
             +" "+str(num_samples)+" samples, "
        velocities = []
        time_spacing = 4*target_speed[component]
        threshold = 0.001
        print "time spacing: "+str(time_spacing)
        for i in range(0,num_samples):
            start_time = self.clock
            self.sendGoal(target_speed)
            #Wait until the quadrotor stops accelerating
            prev_vel = 10000
            while abs(prev_vel-self.groundtruths[component])>threshold:
                prev_vel = self.groundtruths[component]
                #print "prev_time: "+str(prev_vel)+" ground-truth: "+str(self.groundtruths[component])
                time.sleep(0.1)
            #print "target_speed: "+str(target_speed[component])+" ground truth: "+str(self.groundtruths[component])
            velocities.append(target_speed[component] - self.groundtruths[component])
            tmp_target = target_speed
            tmp_target[component] = -target_speed[component]
            self.sendGoal(tmp_target)
            prev_vel = 10000
            while abs(prev_vel-self.groundtruths[component])>threshold:
                prev_vel = self.groundtruths[component]
                time.sleep(0.1)
            velocities.append(target_speed[component] +self.groundtruths[component])
        return velocities
    def velocity_controller_graph(self,component,target_speed):
        data = []
        start_time = self.clock
        time_to_goal = 10.00
        #send the goal
        self.sendGoal(target_speed)
        while self.clock-start_time<time_to_goal:
            data.append({"clock":self.clock,"vel":self.groundtruths[component]})
            time.sleep(0.01)
        return data
