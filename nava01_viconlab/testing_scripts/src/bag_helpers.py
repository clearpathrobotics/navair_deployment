def get_bags(foldername):
    import fnmatch
    import os
    filenames = []
    for root, dirs, files in os.walk(foldername):
        for name in files:
            if fnmatch.fnmatch(name, "*.bag"):
                filename = os.path.join(root, name)
                filenames.append(filename)
    return filenames

def get_time(message,first_time,seconds):
    if message._has_header:
        seconds = message.header.stamp.secs
        nsecs = message.header.stamp.nsecs
        seconds = seconds+nsecs*1e-9-first_time
    return seconds

#this is a diagnostic function to make sure that I actually captured the data I meant to capture
def bag_topics(filename):
    import rosbag
    bag = rosbag.Bag(filename)
    topics = []
    for topic,message,time in bag.read_messages():
        if topic not in topics:
            topics.append(topic)
    return topics
#read through the file to confirm that topics exist.
#will exit as soon as all topics are found
#if some topics are missing, returns false, if topics are available, returns true
def required_topics(filename, topics_list):
    print "Checking Bag file: "+filename
    import rosbag
    bag = rosbag.Bag(filename)
    for topic, message, time in bag.read_messages():
        if topics_list == []:
            next
        if topic in topics_list:
            topics_list.remove(topic)
    for topic in topics_list:
        print "Missing topic: " + topic + " in bagfile: " + filename
    if topics_list == []:
        print "Bagfile " + filename + " contains all required topics"
        return True
    else:
        return False

#guess the name of the uav in the bagfile
def guess_name(filename):
    import rosbag
    bag = rosbag.Bag(filename)
    for topic, message, time in bag.read_messages():
        if topic.find("uav") != -1:
            uav_number = topic.split("/")[1]
            uav_number = uav_number.replace("uav","")
            return uav_number
