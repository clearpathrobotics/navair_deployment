#create a histogram with a chi**2 distribution fitted.
#tmp_data is a list of values for the histogram
#out_filename is the filename for the image file
#x_title is the title on the x axis
def histogram_chi2(tmp_data, out_filename, x_title, units, title=""):
    from numpy import histogram, mean, std, amax, amin, arange
    import matplotlib.pyplot as plt
    import scipy.stats as st
    hist, bin_edges = histogram(tmp_data,range = (mean(tmp_data)-3*std(tmp_data),\
        mean(tmp_data)+3*std(tmp_data)), normed=True,bins=50)
    samples = 100.0
    x_vals = arange(amin(tmp_data),amax(tmp_data), step = (amax(tmp_data)-amin(tmp_data))/samples)
    fig, ax = plt.subplots(1,1)
    fit_vals = st.chi2.fit(tmp_data)
    chi2_mean= fit_vals[0]*fit_vals[2]+fit_vals[1]
    chi2_sd = fit_vals[2]*(2*fit_vals[0])**0.5
    ax.plot(x_vals,st.chi2.pdf(x_vals,fit_vals[0], loc=fit_vals[1],scale=fit_vals[2]),\
        label="Fitted $\chi^2$ Distribution,\n $\sigma$ = "+"{:.3f}".format(chi2_sd)+\
        " Mean = "+"{:.4f}".format(chi2_mean)+" "+units)
    ax.bar(bin_edges[:-1],hist,width = bin_edges[1]-bin_edges[0], color='r', \
        label = "Normalized Histogram ("+str(len(tmp_data))+" samples)")
    ax.set_xlabel(x_title+ " ("+units+")")
    ax.set_ylabel("Frequency")
    ax.legend()
    fig.savefig(out_filename,dpi=100)

#create a histogram with a normal distribution fitted.
#tmp_data is a list of values for the histogram
#out_filename is the filename for the image file
#x_title is the title on the x axis
def histogram_norm(tmp_data, out_filename = 'histogram_norm.png', x_title = 'error', units = 'm', title=None):
    from numpy import histogram, mean, std, amax, amin, arange
    from math import isnan
    import matplotlib.pyplot as plt
    import scipy.stats as st
    #filter out all the NaN's from the array
    tmp_data = [value for value in tmp_data if not isnan(value)]
    hist, bin_edges = histogram(tmp_data,range = (mean(tmp_data)-3*std(tmp_data),\
        mean(tmp_data)+3*std(tmp_data)), normed=True,bins=50)
    samples = 100.0
    x_vals = arange(amin(tmp_data),amax(tmp_data), step = (amax(tmp_data)-amin(tmp_data))/samples)
    fig, ax = plt.subplots(1,1)
    if not title is None:
        ax.set_title(title)
    fit_vals = st.norm.fit(tmp_data)
    ax.plot(x_vals,st.norm.pdf(x_vals, loc=fit_vals[0],scale=fit_vals[1]),\
        label="Fitted Normal Distribution,\n $\sigma$ = "+"{:.2f}".format(fit_vals[1])+\
        " Mean = "+"{:.4f}".format(fit_vals[0])+" "+units)
    ax.bar(bin_edges[:-1],hist,width = bin_edges[1]-bin_edges[0], color='r', \
        label = "Normalized Histogram ("+str(len(tmp_data))+" samples)")
    ax.set_xlabel(x_title+" ("+units+")")
    ax.set_ylabel("Frequency")
    ax.legend()
    fig.savefig(out_filename,dpi=100)

#create a histogram with the SLE parameters highlighted
#tmp_data is a list of values for the histogram
#out_filename is the filename for the image file
def histogram_gen(tmp_data, out_filename = 'histogram_norm.png', x_label = 'Error', unit = 'm', title = None):
    from numpy import histogram, mean, std, amax, amin, arange, argmax
    import matplotlib.pyplot as plt

    hist, bin_edges = histogram(tmp_data, normed=True, bins=50)

    mean = mean(tmp_data)
    sdev = std(tmp_data)
    lower = mean - 3.0*sdev
    upper = mean + 3.0*sdev
    median = bin_edges[argmax(hist)]+0.5*( bin_edges[1]-bin_edges[0])
    fig, ax = plt.subplots(1,1)
    if not title is None:
        ax.set_title(title)
    ax.bar(bin_edges[:-1], hist, width = bin_edges[1]-bin_edges[0], color='r', \
        label = "Normalized Histogram ("+str(len(tmp_data))+" samples)")
    ax.set_ylim(0,1.5*amax(hist))
    ax.axvline(x=mean, label="$\mu$ = {:.3f} ".format(mean)+unit, color='b', linewidth="2")
    ax.axvline(x=median, label="median = {:.3f} ".format(median)+unit, color='c', linewidth="2")
    ax.axvline(x=lower, label="$\mu \pm 3*\sigma$ = ("+"{:.2f}".format(lower)+",{:.2f}) ".format(upper)+unit, color='g', linewidth="2")
    ax.axvline(x=upper, color='g', linewidth="2")
    ax.set_xlabel(x_label)
    ax.set_ylabel("Frequency")
    ax.legend()
    fig.savefig(out_filename, dpi=100)
    return [mean, median, sdev, lower, upper]

#create a histogram with the ARE parameters highlighted
#tmp_data is a list of values for the histogram
#out_filename is the filename for the image file
def sle_graph(tmp_data, out_filename):
    sle_values = histogram_gen(tmp_data, out_filename, "Positional Error (m)", "m")
    return sle_values
#create a histogram with the SLE parameters highlighted
#tmp_data is a list of values for the histogram
#out_filename is the filename for the image file
def are_graph(tmp_data, out_filename):
    are_values = histogram_gen(tmp_data, out_filename, "Rotational Error (deg)", "degrees")
    return are_values

#this needs an n x 3 array with the paths and x/y/z values
def trajectory(paths, labels,out_filename):
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    for i in range(0,len(paths)):
        ax.plot(paths[i][0], paths[i][1], zs=paths[i][2], label=labels[i])
    ax.legend()
    ax.set_xlabel("X position (m)")
    ax.set_ylabel("Y position (m)")
    ax.set_zlabel("Z position (m)")
    #ax.set_zlim(0.0,2.0)
    #ax.set_ylim(-0.5,2.0)
    fig.savefig(out_filename, dpi=100)

def trajectory2d(paths, labels,out_filename):
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1,1)
    for i in range(0,len(paths)):
        ax.plot(paths[i][0], paths[i][1], label=labels[i])
    ax.legend()
    ax.set_xlabel("X position (m)")
    ax.set_ylabel("Y position (m)")
    #ax.set_zlim(0.0,2.0)
    #ax.set_ylim(-0.5,2.0)
    fig.savefig(out_filename, dpi=100)

#the first entry in paths must be the time, the second is the trajectory value
def time_trajectory(paths, labels, out_filename):
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1,1)
    for i in range(0,len(paths)):
        ax.plot(paths[i][0], paths[i][1], label=labels[i])
    ax.legend()
    ax.set_ylabel("X position (m)")
    ax.set_xlabel("Time (s)")
    #ax.set_zlim(0.0,2.0)
    #ax.set_ylim(-0.5,2.0)
    fig.savefig(out_filename, dpi=100)
