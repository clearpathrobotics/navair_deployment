def set_up_ip(master_ip):
    import os
    import socket
    os.environ["ROS_IP"] = socket.getfqdn()
    os.environ["ROS_MASTER_URI"] = master_ip

def check_topic_available(wanted_topics):
    available_topics = get_available_topics()
    #check to make sure that all the topics in wanted_topics
    for topic in wanted_topics:
        if topic not in available_topics:
            print "Topic: "+topic+" not found"
            return False
    #if it made it this far, it found all wanted topics
    return True

def get_available_topics():
    import rosgraph.masterapi
    master = rosgraph.masterapi.Master('/rostopic')
    #we only want the first element of every pair in this list
    topics_full = master.getPublishedTopics('/')
    available_topics = []
    for i in range(0,len(topics_full)):
        available_topics.append(topics_full[i][0])
    return available_topics
    
