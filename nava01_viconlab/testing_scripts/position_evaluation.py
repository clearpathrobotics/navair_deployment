#!/usr/bin/python
import rosparam
import sys
import os

sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/src")
from bag_helpers import *
from graphing_helpers import *
from ros_helpers import *
from test_data import test_data
import socket
import time

if __name__ == "__main__":
    if len(sys.argv)>2:
        config_filename = sys.argv[1]
    else:
        print "No configuration file specified, loading default values"
        config_filename = "variables.yaml"
    #load in the test parameters from the yaml file
    data = rosparam.load_file(config_filename)[0][0]
    yaw_test = data['yaw_test']
    position_test = data['position_test']
    height = float(data['height'])
    _uav_number = int(data['uav_number'])
    num_points = int(data['num_points'])
    num_angles = int(data['num_angles'])
    local_machine = data['local_machine']
    exterior_ip = data['exterior_ip']
    volume = []
    parts = ['x','y','z']
    for part in parts:
        limits = data['volume_limits'][part]
        volume.append([float(limits['min']),float(limits['max'])])
    if local_machine:
        tmp_ip = socket.getfqdn()
        set_up_ip("http://"+tmp_ip+":11311")
    else:
        set_up_ip("http://"+exterior_ip+":11311")

    #create the test object
    print "Connecting to quadrotor "+str(_uav_number)
    test = test_data(verbose = False, uav_number = _uav_number)
    if test.position_mode:
        time.sleep(2)
        test.serverCheck()
    print "Finished Server Check, Taking off"
    test.takeoffGoal()
    print "Successfully took off"
    if yaw_test:
        print "Initiating Yaw Test"
        angle_errors = test.angle_grid_test(num_angles,height)
        out_filename = "yaw_hold_results.png"
        x_title = "Angular Error"
        title = "Yaw Hold test Results"
        units = "rad"
        histogram_norm(angle_errors, out_filename, x_title, units)
    if position_test:
        print "Initiating position test"
        position_hold_results = test.position_grid_test(volume, num_points)
        #create histogram
        out_filename = "position_hold_results.png"
        x_title = "Positional Error"
        units = "m"
        title = "Position Hold Test Results"
        tmp_data = [item for sublist in position_hold_results for item in sublist]
        histogram_chi2(tmp_data, out_filename, x_title, units, title)
    print "landing"
    test.landGoal()
